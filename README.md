# VISTAL BANK


## Getting started
Empowering your financial journey

## Add your files

 https://www.figma.com/file/ukmwfQPIizbAy2quTKqsW2/VISTA?type=design&node-id=0%3A1&t=cGKxQwGtAY0qDu8j-1


## Collaborate with your team

hr@veegil.com


## Description
Effortlessly track your income, expenses, and savings all in one place with VISTAL BANK by creating account as a user and then register by inputing your details and verifying with a code and then fill in your information, confirm with your phone number which is your account number. For users that has created an account with VISTAL BANK will login to account and you input their pin and then it will take them down to the home page where they can make online payment, users can make transactions to people and also make deposit . In the dashboard users can track their transaction history. In the transfer page, users can make transfers to vistal account home and aboad, then select the recepient they are transacting with and confirm transaction. 

Experience the future of banking with VISTAL BANK. Simplify your finances, make informed decisions, and achieve your financial goals with ease. Download the app today and embark on a journey towards financial empowerment. 
